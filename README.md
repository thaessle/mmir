# MIAGE Messenger in React

Our objective is to build a simple chat app
![MMIR](./Project.png)

## Training
### Introduction
The Document Object Model (DOM) is a programming interface for HTML, XML and SVG documents. The DOM provides a representation of the document as a structured group of nodes and objects that have properties and methods. Nodes can also have event handlers attached to them, and once that event is triggered the event handlers get executed. Essentially, it connects web pages to scripts or programming languages.

The main idea of react is to compose a web application with components.  

The DOM has 2 main problems :
* lack of encapsulation
  * Web components & Angular solution is Shadow DOM
  * React solution is react component
* low performance to alter three nodes
  * React use Virtual DOM : The Virtual DOM is an abstraction of the HTML DOM. It is lightweight and detached from the browser-specific implementation details. Since the DOM itself was already an abstraction, the virtual DOM is, in fact, an abstraction of an abstraction.
  * There are other implementations of virtual DOM (Mithril, Inferno, Vue.js, ...)

### React

React is a declarative, efficient, and flexible JavaScript library for building user interfaces.
React has a few different kinds of components, but we'll start with "Stateless components"

A Stateless component is just a function returning JSX

``` js
function myComponent(){
  return <div className="members-list">
            <h1> A list of members </h1>
            <ul>
              <li>Elliott</li>
              <li>Darlene</li>
            </ul>
          </div>
}
```
This funny tag syntax is neither a string nor HTML.

It is called [JSX](https://facebook.github.io/react/docs/introducing-jsx.html), and it is a syntax extension to JavaScript. JSX may remind you of a template language, but it comes with the full power of JavaScript.

JSX contains regular HTML tags or other React components.
There are few differences, but you can notice that *class* property is replace by *className*

React is a UI library, we will consider "state of art" but remember there are lot of way to use it.

### What you need to start :

Clone the project

Install the dependencies : `npm i`

Let's hacking : `npm run dev`

### Time to start
At this moment only consider the ./src folder. Inside you have :
* assets folder : contains static assets
* components folder : contain a folder for each reat component
  * each component is compose of an `index.js` file and may also have unit tests in an `index.test.js` file
* index.html : HTML skeleton of the project (you shouldn't alter it)
* index.js : bootstrap your react app

``` js
import React from 'react';
import ReactDOM from 'react-dom'; //React entry point of the DOM-related rendering paths
import App from './components/app'; //main component

ReactDOM.render(
  <App/>,
  document.getElementById('main')
); // bootstrap <App/> component to the DOM element with id 'main'
```
### Thinking in React
![MMIR](./React-Project.png)
Each rectangle represent a React Component.
We can represent a three like :

``` XML
<App> {/* A JSX comment */}
  <Title/>{/* Yellow top component */}
  <Users>{/* Purple left component */}
    <User>{/* Blue left component */}
      <Picture/>{/* Red left component */}
      <Name/>{/* Green left component */}
      <Unread/>{/* Brown left component */}
    </User>
  </Users>
  <Timeline>{/* Orange right component */}
    <Message/>{/* Pink right component */}
  </Timeline>
  <Formular>{/* Purple bottom component */}
    <Input/>{/* Light Green bottom component */}
    <Submit/>{/* Dark Green bottom component */}
  </Formular>
</App>  
```

### Exercice
For the exercice, we will not have a database so you can hardcode some fake one ... wait until next week to plug DB.

You can already take a look at Tite Component

``` JS
import React from 'react';

function Title ({children}) {
  return(
      <h1>
            {children}
      </h1>
);}


Title.propTypes = {
  children: React.PropTypes.node,
};

export default Title;
```

Even in ES6, React gives you a way to [typecheck](https://facebook.github.io/react/docs/typechecking-with-proptypes.html) your components properties. It's a good practice to use it. If you want typecheck the whole application you can use Flow or Typescript but we will not use them here.

1. We will create the Picture component. It should take a string URL and string alternative as [properties](https://facebook.github.io/react/docs/components-and-props.html). Remeber you must achieve a functional component.
2. Create all components for left & right areas
3. Add unit tests with [enzyme](https://facebook.github.io/react/docs/components-and-props.html), Mocha & Chai
4. Even if you can use CSS (or LESS or SASS), it's often prefer to use inline style (styling inside the JS). Because it may be annoying I advice you to use a library. We will use [Styled Components](https://github.com/styled-components/styled-components) to style our components.
5. Create Right component and use [React Router](https://reacttraining.com/react-router/) to manage routes in a single app way (only right part should be update ... and page refresh should work).
6. Take a look at [webpack config](./webpack.config.js) to understand how the bundle work and ask your questions.

### Libraries to use in this project :
* [Babel](https://babeljs.io/) : JavaScript Polyfill for ES2015, ES2016, ES2017
* [React](https://facebook.github.io/react/) : Fast, unopinionated, minimalist web framework for node.js
* [react-router](https://facebook.github.io/react/) : routing library for react
* [styled-component](styled-components) : visual primitives for the components
* [webpack](https://webpack.github.io/docs/) : module builder
* Unit test kit :
  * [mocha](https://mochajs.org/) :  test framework running on node.js and in the browser
  * [chai](http://chaijs.com/) : assertion library
  * [enzyme](http://airbnb.io/enzyme/) : testing utility for React
  * [nyc](https://github.com/istanbuljs/nyc) : Istanbul command line interface

### NPM Scripts :
* Run unit tests : `npm run test`
* Run linters : `npm run lint`
* Run platform for dev with hot reload : `npm run dev` => http://localhost:8080
* Build platform for production : `npm run build`
* Run platform for production : `npm start` => http://localhost:3000

### Usefull links :
* [Course](https://egghead.io/courses/react-fundamentals) : understand in details

![80's hacker](http://i.giphy.com/26vUBzVWEBkk3KD6M.gif)
